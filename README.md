# Sobre el proyecto

Es un proyecto que se encarga del envio de correo hacia cualquier servico de mesajeria (Gmail, Hotmail, Yahoo, etc. ) usando una plantilla html 
### Plantilla
![plantilla](https://gitlab.com/Christopher_JS/send-email/-/raw/master/public/images/plantilla.PNG)

## Libreria usada

#### Nodemailer
Nodemailer es un módulo para aplicaciones Node.js que permite el envío de correos electrónicos de manera fácil.

Puede obtener más información en la documentación [Enviar correo con NodeJS](https://facebook.github.io/create-react-app/docs/getting-started).
# Scripts disponibles
En el directorio del proyecto, puede ejecutar:

### `npm start`
Ejecuta la aplicación en modo de desarrollo.
Abra http://localhost:3000 para verlo en el navegador.

La página se volverá a cargar si realiza modificaciones.
También verá cualquier error en la consola.

# Aprende más
Puede obtener más información en la documentación [Crear aplicación de React](https://facebook.github.io/create-react-app/docs/getting-started).

Para aprender React, consulte la [documentación de React ](https://reactjs.org/).

#  Deployment

La aplicacion fue desplegada en AWS

## AWS
Amazon Web Services (AWS) es la plataforma en la nube más adoptada y completa en el mundo, que ofrece más de 175 servicios integrales de centros de datos a nivel global. Millones de clientes, incluyendo las empresas emergentes que crecen más rápido, las compañías más grandes y los organismos gubernamentales líderes, están utilizando AWS para reducir los costos, aumentar su agilidad e innovar de forma más rápida. [Más información ](https://aws.amazon.com/es/what-is-aws/)

## Ruta de despliegue

http://sendemail-env.eba-zenm5pjf.us-east-2.elasticbeanstalk.com/

# Uso

## Local

- Para ser uso del servicio de forma local, deben de ejecutar el proyecto con el comando `npm start`.
- Luego usar cualquier herramienta que le permita hacer peticiones HTTP, un ejemplo es una aplicación que se llama  [Postman ](https://www.postman.com/).
- Si la petición sale bien , se envia un estatus 200 y se retorna el correo la cual fue enviado el email
- Si la petición sale Mal , se envia un estatus 500 y se retorna el error

#### Ruta tipo `POST`
http://localhost:3000/api/send-email
#### Dato
{
    to: < correo >
}
Reemplazar < correo > por un email valido

## AWS

- Solo deben acceder a la ruta.
- Luego usar cualquier herramienta que le permita hacer peticiones HTTP, un ejemplo es una aplicación que se llama Postman.
- Si la petición sale bien , se envia un estatus 200 y se retorna el correo la cual fue enviado el email
- Si la petición sale Mal , se envia un estatus 500 y se retorna el error

#### Ruta tipo `POST`
http://SendEmail-env.eba-zenm5pjf.us-east-2.elasticbeanstalk.com/api/send-email
#### Dato
{
    to: < correo >
}
Reemplazar < correo > por un email valido

